To deploy an nfs server:

1. Configure target machine to allow passwordless root ssh

2. Edit ansible/hosts ansible_host variable

3. Run:
```
ansible-playbook -v nfs-server.yml -u root
```

This playbook was tested in a Ubuntu machine